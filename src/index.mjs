const orElse = Symbol();

class NonExhaustiveMatch extends Error {
    constructor(message) {
        super(message);
        this.name = "NonExhaustiveMatch";
    }
}

const handlePredicate = (predicate, data) => {
    if (predicate === orElse)
        return true;

    if (predicate instanceof Function)
        return predicate(data);

    if (predicate === data)
        return true;
};

const handleResultant = (resultant, data) => {
    if (resultant instanceof Function)
        return resultant(data);

    return resultant;
};

const splitIntoBranches = (list) => {
    const branches = [];
    for (let i = 0; i < list.length; i += 2) {
        branches.push(list.slice(i, i + 2));
    }
    return branches;
};

const match = (...branches) => (data) => {
    if (branches[0] && !Array.isArray(branches[0]))
        branches = splitIntoBranches(branches);

    const matchingBranch = branches.find(([predicate]) => handlePredicate(predicate, data));
    
    if (matchingBranch === undefined)
        throw new NonExhaustiveMatch(`Match statement was not able to find a predicate for the data ${data}`);

    const [_, resultant] = matchingBranch;

    return handleResultant(resultant, data);
};

const isA = (clazz) => (instance) => clazz.prototype.isPrototypeOf(instance);
const anyOf = (...list) =>
    (instance) => list.some(predicate => handlePredicate(predicate, instance));
const id = x => x;

export{ match as default, orElse, isA, anyOf, id, NonExhaustiveMatch }