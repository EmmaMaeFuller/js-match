import match, { orElse, isA, anyOf, id } from '../src/index.mjs'
import { strict as assert } from 'node:assert'

let passed = 0;
let failed = 0;

const isVerbose = process.argv.includes('--verbose');

function it(description, code) {
    try {
        code();
        console.log('\x1b[36m%s\x1b[0m', `✓ ${description}`);
        passed++;
    } catch (e) {
        failed++;
        console.log('\x1b[31m%s\x1b[0m', `✗ ${description}`);
        console.log(isVerbose ? e : e.message);
    }
}

it("Handles literal values as predicates", () => {
    const matchDirection = match(
        "north",   "You picked north",
        "south",   "You picked south"
    );

    assert.equal(matchDirection("south"), "You picked south");
})

it("Handles functions as predicates", () => {
    const isEven = match(
        x => x % 2 === 0,  true,
        x => x % 2 === 1,  false
    );

    assert.equal(isEven(7), false);
})

it("Allows specification of a wildcard branch", () => {
    const getPrice = match(
        "Pepperoni",    2.50,
        "Cheese",       1.00,
        orElse,         9.99
    );

    assert.equal(getPrice("Buffalo Chicken"), 9.99);
})

it("Handles functions as resultants", () => {
    const greet = match(
        "Emma",     "Hey Emma!! Great to see you, how're the cats?",
        orElse,     name => `Hey, ${name}! I'm not sure we've met`
    );

    assert.equal(greet("Jeremy"), "Hey, Jeremy! I'm not sure we've met");
});

it("Requires match statement to be exhaustive", () => {
    const printNumber = match(
        1,   "One",
        2,   "Two"
    );

    assert.throws(() => printNumber(3), { name: 'NonExhaustiveMatch' });
});

it("Exposes isA helper method", () => {
    class Dog { constructor(name) { this.name = name; } }

    const isAGoodBoy = match(
        isA(Dog),  "Absolutely",
        orElse,    "Maybe"
    );

    const fido = new Dog("Fido");
    assert.equal(isAGoodBoy(fido), "Absolutely");
})

it("Exposes anyOf helper method", () => {
    const isEmmasCat = match(
        anyOf("Diddle", "Ollie"),    true,
        orElse,                      false
    );

    assert.equal(isEmmasCat("Ollie"), true);
    assert.equal(isEmmasCat("Chowder"), false);

    class Horse {}
    class Zebra {}

    const hasHooves = match(
        anyOf(isA(Horse), isA(Zebra)),  true,
        orElse,                         false
    );

    assert.equal(hasHooves(new Horse()), true);
    assert.equal(hasHooves("Turtle"), false);
})

it("Exposes id helper method", () => {
    const fibb = match(
        anyOf(0, 1),      id,
        orElse,           x => fibb(x - 1) + fibb(x - 2)
    );

    assert.equal(fibb(10), 55);
})

it("Allows usage with or without square brackets", () => {
    const withBrackets = match(
        [1, 'a'],
        [2, 'b'],
        [3, 'c']
    );

    const withoutBrackets = match(
        1,  'a',
        2,  'b',
        3,  'c'
    );

    assert.equal(withBrackets(1), withoutBrackets(1));
    assert.equal(withBrackets(2), withoutBrackets(2));
    assert.equal(withBrackets(3), withoutBrackets(3));
})

console.log(`\nRan ${passed + failed} tests: \x1b[36m${passed} passed\x1b[0m` + (failed > 0 ? `, \x1b[31m${failed} failed\x1b[0m` : ''));
process.exit(failed > 0 ? 1 : 0);