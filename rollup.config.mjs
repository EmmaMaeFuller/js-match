import terser from '@rollup/plugin-terser';

export default {
    input: 'src/index.mjs',
    output: {
      file: 'dist/bundle.mjs'
    },
    plugins: [terser()]
  };